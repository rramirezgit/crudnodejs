const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/User');

//Verificar Usuario y contraseña
passport.use(new LocalStrategy({
    usernameField: 'email',
}, async(email, password, done) => {
    const user = await User.findOne({ email: email });
    if (!user) {
        return done(null, false, { message: 'Usuario no encontrado' });
    } else {
        const match = await user.matchPassword(password);
        if (match) {
            return done(null, user);
        } else {
            return done(null, false, { message: 'Los datos no coinciden' });
        }
    }
}));

//Almacenar usuario en session
passport.serializeUser((user, done) => {
    done(null, user.id);
});

//buscar usuario por id
passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});