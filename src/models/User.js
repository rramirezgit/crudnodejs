const mongoose = require('mongoose');
const { Schema } = mongoose;
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    date: { type: Date, default: Date.now }
});

//Cifrar contraseña que ingresa el usuario al registrarse
UserSchema.methods.encryptPassword = async(password) => {
    const salt = await bcrypt.genSalt(10);
    const hash = bcrypt.hash(password, salt);
    return hash;
};

/* Funcion para comparar la contraseña ingresa por el usuario contra la almacena en BD */
/* La funcion flecha pierde el scope, es decir no se puede usar el this del UserSchema */
UserSchema.methods.matchPassword = async function(password) { 
    return await bcrypt.compare(password, this.password);
}

module.exports = mongoose.model('User', UserSchema);