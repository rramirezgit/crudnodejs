const express = require('express');
const router = express.Router();
const Note = require('../models/Note');
const path = require('path');
const multer = require('multer');
const { isAuthenticated } = require('../helpers/auth');

//Set Storage
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../public/files'))
    },

    // By default, multer removes file extensions so let's add them back
    filename: function (req, file, cb) {
        cb(null, file.filename + '-' + Date.now() + path.extname(file.originalname));
    }
});

//Configuracion del storage 
const upload = multer({ storage })


router.get('/notes', isAuthenticated, async (req, res) => {
    const notes = await Note.find({ user: req.user.id }).sort({ date: 'desc' });
    res.render('notes/all-notes', { notes });
});

router.get('/notes/add', isAuthenticated, (req, res) => {
    res.render('notes/new-note');
});

router.post('/notes/new-note', upload.single('image'), isAuthenticated, async (req, res) => {
    const { title, description } = req.body;
    const image = req.file;
    console.log(image)
    const errors = [];
    if (!title) {
        errors.push({ text: "Por favor escriba un titulo" });
    }
    if (!description) {
        errors.push({ text: "Por favor escriba una descripcion" });
    }
    if (errors.length > 0) {
        res.render('notes/new-note', {
            errors,
            image,
            title,
            description
        });
    } else {
        let fileUrl = '';
        if (image) {
            fileUrl = 'files/' + image.filename;
        }

        const newNote = new Note({ title, description, fileUrl });
        newNote.user = req.user.id;
        await newNote.save();
        req.flash('success_msg', 'Nota Agregada Satisfactoriamente');
        res.redirect('/notes');
    }
});

router.get('/notes/edit/:id', isAuthenticated, async (req, res) => {
    const note = await Note.findById(req.params.id);
    res.render('notes/edit-note', { note });
});

router.put('/notes/edit-note/:id', isAuthenticated, async (req, res) => {
    const { title, description } = req.body;
    const image = req.file;
    console.log(image)
    let fileUrl = '';
    if (image) {
        fileUrl = 'files/' + image.filename;    }

    await Note.findByIdAndUpdate(req.params.id, { title, description, fileUrl });
    req.flash('success_msg', 'Nota Actualizada Satisfactoriamente');
    res.redirect('/notes');
});

router.delete('/notes/delete/:id', isAuthenticated, async (req, res) => {
    await Note.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'Nota Eliminada Satisfactoriamente');

    res.redirect('/notes');
});

module.exports = router;